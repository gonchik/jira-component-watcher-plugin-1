package com.burningcode.jira.rest;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement
public class ComponentWatcherList {
    @XmlElement
    private String projectKey;

    @XmlElement
    private String projectName;

    @XmlElement
    private String component;

    @XmlElement
    private ArrayList<ComponentWatcher> watchers;

    // This private constructor isn't used by any code, but JAXB requires any
    // representation class to have a no-args constructor.
    private ComponentWatcherList() {}

    public ComponentWatcherList(final String projectKey, final String projectName, final String component, final ArrayList<ComponentWatcher> watchers) {
        this.projectKey = projectKey;
        this.projectName = projectName;
        this.component = component;
        this.watchers = watchers;
    }
}
