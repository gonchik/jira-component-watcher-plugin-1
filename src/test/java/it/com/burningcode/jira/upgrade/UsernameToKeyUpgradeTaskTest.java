package it.com.burningcode.jira.upgrade;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.LoginAs;
import it.com.burningcode.jira.BaseIntegrationTest;
import org.junit.Before;
import org.junit.Test;

@LoginAs(user = "admin")
public class UsernameToKeyUpgradeTaskTest extends BaseIntegrationTest {
    @Before
    public void setup() {
    }

    @Test
    @Restore("WithRenamedUsernames.zip")
    public void testUpgrade() {
        final Long componentId = 10100L;

        // Check that the non-renamed usernames exist
        for(int i = 1; i <= 10; i++) {
            final String username = "test" + i;
            final String displayName = "Test " + i;

            gotoProjectComponentWatchers("TSTUPG");

            tester.assertElementPresent("component_" + componentId + "_user_watcher_" + username);

            gotoEditUserWatchersScreen(componentId);

            assertions.getTextAssertions().assertTextPresent(displayName + " (" + username + ")");
            tester.assertFormElementPresent("removeWatcher_" + username);
        }

        // Check that renamed usernames we're migrated
        for(int i = 11; i <= 100; i++) {
            final String username = "test" + i + "_new";
            final String displayName = "Test " + i;

            gotoProjectComponentWatchers("TSTUPG");

            tester.assertElementPresent("component_" + componentId + "_user_watcher_" + username);

            gotoEditUserWatchersScreen(componentId);

            assertions.getTextAssertions().assertTextPresent(displayName + " (" + username + ")");
            tester.assertFormElementPresent("removeWatcher_" + username);
        }
    }
}
