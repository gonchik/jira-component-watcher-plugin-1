package it.com.burningcode.jira.plugin;

import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.notification.type.NotificationType;
import com.burningcode.jira.plugin.settings.NotificationTypeSetting;
import it.com.burningcode.jira.BaseIntegrationTest;
import org.junit.Test;

@LoginAs(user = "admin")
public class ComponentWatcherSettingsTest extends BaseIntegrationTest {
    public void assertNotificationTypeSet(final String displayName) {
        navigation.gotoResource("ComponentWatcherSettings.jspa");
        assertions.getTextAssertions().assertTextPresent(displayName);
        tester.clickLink("edit_component_watcher_settings");
        assertions.forms().assertSelectElementHasOptionSelected("notificationType", displayName);
    }

    @Test
    @RestoreBlankInstance
    public void testDefaultNotificationType() {
        assertNotificationTypeSet("All Watchers");
    }

    @Test
    @RestoreBlankInstance
    public void testChangeNotificationType() throws Exception {
        final String notificationType = NotificationType.COMPONENT_LEAD.dbCode();
        setComponentWatcherNotificationType(notificationType);
        assertNotificationTypeSet("Component Lead");
    }

    @Test
    @RestoreBlankInstance
    public void testNoNotificationType() throws Exception {
        setComponentWatcherNotificationType(NotificationTypeSetting.NO_NOTIFICATION_CODE);
        assertNotificationTypeSet("None");
    }
}
